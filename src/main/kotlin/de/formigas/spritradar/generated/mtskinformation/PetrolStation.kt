//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.11.14 at 02:59:49 PM CET 
//
package de.formigas.spritradar.generated.mtskinformation

import javax.xml.bind.annotation.*

/**
 * Petrol station
 *
 *
 * Java class for PetrolStation complex type.
 *
 *
 * The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="PetrolStation">
 * &lt;complexContent>
 * &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 * &lt;sequence>
 * &lt;element name="petrolStationVersionTime" type="{http://mtskInformation/schema/01-01-00}String"/>
 * &lt;element name="petrolStationName" type="{http://mtskInformation/schema/01-01-00}String"/>
 * &lt;element name="petrolStationBrand" type="{http://mtskInformation/schema/01-01-00}String" minOccurs="0"/>
 * &lt;element name="petrolStationStreet" type="{http://mtskInformation/schema/01-01-00}String" minOccurs="0"/>
 * &lt;element name="petrolStationHouseNumber" type="{http://mtskInformation/schema/01-01-00}String" minOccurs="0"/>
 * &lt;element name="petrolStationPostcode" type="{http://mtskInformation/schema/01-01-00}String" minOccurs="0"/>
 * &lt;element name="petrolStationPlace" type="{http://mtskInformation/schema/01-01-00}String" minOccurs="0"/>
 * &lt;element name="petrolStationPublicHolidayIdentifier" type="{http://mtskInformation/schema/01-01-00}FederalStateEnum" minOccurs="0"/>
 * &lt;element name="openingTimes" type="{http://mtskInformation/schema/01-01-00}Period" maxOccurs="unbounded"/>
 * &lt;element name="petrolStationLocation" type="{http://mtskInformation/schema/01-01-00}PointCoordinates"/>
 * &lt;element name="petrolStationExtension" type="{http://mtskInformation/schema/01-01-00}_ExtensionType" minOccurs="0"/>
 * &lt;/sequence>
 * &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 * &lt;attribute name="version" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 * &lt;/restriction>
 * &lt;/complexContent>
 * &lt;/complexType>
</pre> *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
    name = "PetrolStation",
    propOrder = ["petrolStationVersionTime", "petrolStationName", "petrolStationBrand", "petrolStationStreet", "petrolStationHouseNumber", "petrolStationPostcode", "petrolStationPlace", "petrolStationPublicHolidayIdentifier", "openingTimes", "petrolStationLocation", "petrolStationExtension"]
)
class PetrolStation {
    /**
     * Gets the value of the petrolStationVersionTime property.
     *
     * @return possible object is
     * [String]
     */
    /**
     * Sets the value of the petrolStationVersionTime property.
     *
     * @param value allowed object is
     * [String]
     */
    @XmlSchemaType(name = "string")
    var petrolStationVersionTime: String? = null
    /**
     * Gets the value of the petrolStationName property.
     *
     * @return possible object is
     * [String]
     */
    /**
     * Sets the value of the petrolStationName property.
     *
     * @param value allowed object is
     * [String]
     */
    var petrolStationName: String? = null
    /**
     * Gets the value of the petrolStationBrand property.
     *
     * @return possible object is
     * [String]
     */
    /**
     * Sets the value of the petrolStationBrand property.
     *
     * @param value allowed object is
     * [String]
     */
    var petrolStationBrand: String? = null
    /**
     * Gets the value of the petrolStationStreet property.
     *
     * @return possible object is
     * [String]
     */
    /**
     * Sets the value of the petrolStationStreet property.
     *
     * @param value allowed object is
     * [String]
     */
    var petrolStationStreet: String? = null
    /**
     * Gets the value of the petrolStationHouseNumber property.
     *
     * @return possible object is
     * [String]
     */
    /**
     * Sets the value of the petrolStationHouseNumber property.
     *
     * @param value allowed object is
     * [String]
     */
    var petrolStationHouseNumber: String? = null
    /**
     * Gets the value of the petrolStationPostcode property.
     *
     * @return possible object is
     * [String]
     */
    /**
     * Sets the value of the petrolStationPostcode property.
     *
     * @param value allowed object is
     * [String]
     */
    var petrolStationPostcode: String? = null
    /**
     * Gets the value of the petrolStationPlace property.
     *
     * @return possible object is
     * [String]
     */
    /**
     * Sets the value of the petrolStationPlace property.
     *
     * @param value allowed object is
     * [String]
     */
    var petrolStationPlace: String? = null
    /**
     * Gets the value of the petrolStationPublicHolidayIdentifier property.
     *
     * @return possible object is
     * [FederalStateEnum]
     */
    /**
     * Sets the value of the petrolStationPublicHolidayIdentifier property.
     *
     * @param value allowed object is
     * [FederalStateEnum]
     */
    @XmlSchemaType(name = "string")
    var petrolStationPublicHolidayIdentifier: FederalStateEnum? = null

    @XmlElement(required = true)
    var openingTimes: List<Period>? = null
    /**
     * Gets the value of the petrolStationLocation property.
     *
     * @return possible object is
     * [PointCoordinates]
     */
    /**
     * Sets the value of the petrolStationLocation property.
     *
     * @param value allowed object is
     * [PointCoordinates]
     */
    @XmlElement(required = true)
    var petrolStationLocation: PointCoordinates? = null
    /**
     * Gets the value of the petrolStationExtension property.
     *
     * @return possible object is
     * [ExtensionType]
     */
    /**
     * Sets the value of the petrolStationExtension property.
     *
     * @param value allowed object is
     * [ExtensionType]
     */
    var petrolStationExtension: ExtensionType? = null
    /**
     * Gets the value of the id property.
     *
     * @return possible object is
     * [String]
     */
    /**
     * Sets the value of the id property.
     *
     * @param value allowed object is
     * [String]
     */
    @XmlAttribute(name = "id", required = true)
    var id: String? = null
    /**
     * Gets the value of the version property.
     *
     * @return possible object is
     * [String]
     */
    /**
     * Sets the value of the version property.
     *
     * @param value allowed object is
     * [String]
     */
    @XmlAttribute(name = "version", required = true)
    var version: String? = null

}
