//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.11.15 at 09:24:06 PM CET 
//
package de.formigas.spritradar.generated.mtskinformation

import javax.xml.bind.annotation.*

/**
 *
 * Java class for _PetrolStationVersionedReference complex type.
 *
 *
 * The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="_PetrolStationVersionedReference">
 * &lt;complexContent>
 * &lt;extension base="{http://mtskInformation/schema/01-00-00}VersionedReference">
 * &lt;attribute name="targetClass" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" fixed="PetrolStation" />
 * &lt;/extension>
 * &lt;/complexContent>
 * &lt;/complexType>
</pre> *
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "_PetrolStationVersionedReference")
class PetrolStationVersionedReference : VersionedReference() {
    @XmlAttribute(name = "targetClass", required = true)
    @XmlSchemaType(name = "anySimpleType")
    var targetClass: String? = null
}
