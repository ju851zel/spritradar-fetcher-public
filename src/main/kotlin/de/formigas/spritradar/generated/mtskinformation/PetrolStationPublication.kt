//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.11.14 at 10:18:47 PM CET 
//
package de.formigas.spritradar.generated.mtskinformation

import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlAccessType
import de.formigas.spritradar.generated.mtskinformation.GenericPublication
import de.formigas.spritradar.generated.mtskinformation.PetrolStationPublication
import javax.xml.bind.annotation.XmlElement
import java.util.ArrayList
import javax.xml.bind.annotation.XmlType

/**
 * PetrolStationPublication
 *
 *
 * Java class for PetrolStationPublication complex type.
 *
 *
 * The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="PetrolStationPublication">
 * &lt;complexContent>
 * &lt;extension base="{http://mtskInformation/schema/01-00-00}GenericPublication">
 * &lt;sequence>
 * &lt;element name="petrolStation" type="{http://mtskInformation/schema/01-00-00}PetrolStation" maxOccurs="unbounded"/>
 * &lt;/sequence>
 * &lt;/extension>
 * &lt;/complexContent>
 * &lt;/complexType>
</pre> *
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PetrolStationPublication", propOrder = ["petrolStation"])
class PetrolStationPublication : de.formigas.spritradar.generated.mtskinformation.GenericPublication() {
    @XmlElement(required = true)
     var petrolStation: List<PetrolStation>? = listOf()

}
