package de.formigas.spritradar.fetcher

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.module.kotlin.readValue
import de.formigas.spritradar.generated.mtskinformation.D2LogicalModel
import javax.xml.stream.XMLInputFactory

const val nameSpaceDefinition = """xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance""""
const val schemaLocationV1x0x0 =
    """xsi:schemaLocation="http://mtskInformation/schema/01-00-00 PetrolStationPublication.xsd""""
const val schemaLocationV1x1x0 =
    """xsi:schemaLocation="http://mtskInformation/schema/01-01-00 PetrolStationPublication.xsd""""

class XMLParser {

    fun parse(payloadAsStrings: List<String>): List<D2LogicalModel> {

        val parsedModel = parsePayload(payloadAsStrings, null)

        val parsedModelMissingNameSpace = parsePayloadWithFixedMissingNameSpaceDefinition(
            parsedModel.failed.map { it.second }, schemaLocationV1x0x0
        )

        val parsedModelMissingNameSpace1 = parsePayloadWithFixedMissingNameSpaceDefinition(
            parsedModelMissingNameSpace.failed.map { it.second },
            schemaLocationV1x1x0,
        )

        val parsedModelUnknownNamespace = parsePayloadWithFixedUnknownNamespace(
            parsedModelMissingNameSpace1.failed.map { it.second }, "d2ns"
        )

        val parsedModelUnknownNs0Namespace = parsePayloadWithFixedUnknownNamespace(
            parsedModelUnknownNamespace.failed.map { it.second }, "ns0"
        )


        //TODO: Some parsing fails because the ordering of payloadPublication is wrong.
        //      The xsi:type attribute must be before lang and id for valid parsing (don't ask me why)
        //      E.g. INVALID: <payloadPublication lang="de" xsi:type="PetrolStationPublication" id="P-2021-0000030475">
        //      E.g. VALID: <payloadPublication xsi:type="PetrolStationPublication" lang="de" id="P-2021-0000030475">
        val result = ParsedModel(
            parsedModel.parsed + parsedModelMissingNameSpace.parsed + parsedModelMissingNameSpace1.parsed + parsedModelUnknownNamespace.parsed + parsedModelUnknownNs0Namespace.parsed,
            parsedModelUnknownNs0Namespace.failed,
        )
        println("parsed count:" + result.parsed.count())
        println("failed count:" + result.failed.count())
        result.failed.forEach {
            println("""Exception: ${it.first}""")
            println("""XML string that failed (first 200 chars.): ${it.second.subSequence(0, 200)}""")
        }
        return result.parsed
    }


    private fun parsePayloadWithFixedMissingNameSpaceDefinition(
        payloadAsStrings: List<String>,
        schemaLocation: String,
    ): ParsedModel = parsePayload(payloadAsStrings) {
        if (it.contains(nameSpaceDefinition).not() && it.contains(schemaLocation)) {
            it.replace(schemaLocation, "$nameSpaceDefinition $schemaLocation")
        } else it
    }

    private fun parsePayloadWithFixedUnknownNamespace(
        payloadAsStrings: List<String>,
        nameSpace: String,
    ): ParsedModel = parsePayload(payloadAsStrings) {
        if (it.contains(":$nameSpace")) {
            it.replace(":$nameSpace", "").replace("$nameSpace:", "")
        } else it
    }

    private fun parsePayload(
        payloadAsStrings: List<String>, modifyXmlBeforeParsing: ((String) -> String)?
    ): ParsedModel {
        val success: MutableList<D2LogicalModel> = mutableListOf()
        val failed: MutableList<Pair<Throwable, String>> = mutableListOf()
        payloadAsStrings.forEach {
            var xmlAsString = modifyXmlBeforeParsing?.invoke(it) ?: it
            if (xmlAsString.contains("recurringTimePeriodOfDay")) {
                xmlAsString = xmlAsString.replace("<openingTimes>", "<openingTimes type=\"PeriodV1x1x0\">")
            }
            if (xmlAsString.contains("startOfPeriod")) {
                xmlAsString = xmlAsString.replace("<openingTimes>", "<openingTimes type=\"PeriodV1x0x0\">")
                xmlAsString = xmlAsString.replace("<overrideClosed>", "<overrideClosed type=\"PeriodV1x0x0\">")
                xmlAsString = xmlAsString.replace("<overrideOpen>", "<overrideOpen type=\"PeriodV1x0x0\">")
            }

            val result: Result<D2LogicalModel> = generateParser().runCatching { readValue(xmlAsString) }
            if (result.isSuccess) success.add(result.getOrNull()!!)
            else failed.add(Pair(result.exceptionOrNull()!!, xmlAsString))
        }
        return ParsedModel(success, failed)
    }

    private fun generateParser(): XmlMapper =
        XmlMapper(JacksonXmlModule().apply { setDefaultUseWrapper(false) }).apply {
            disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
            enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS)
            factory.xmlInputFactory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true)
        }
}

private data class ParsedModel(
    val parsed: List<D2LogicalModel>,
    val failed: List<Pair<Throwable, String>>,
)
