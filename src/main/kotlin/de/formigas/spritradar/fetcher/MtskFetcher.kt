package de.formigas.spritradar.fetcher

import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import de.formigas.spritradar.generated.mtskinformation.D2LogicalModel
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.apache.commons.codec.binary.Base64
import java.nio.charset.StandardCharsets
import java.util.zip.GZIPInputStream
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.X509TrustManager

private const val mdmHost = "broker.mdm-portal.de"
private const val patrolStationSubscriptionID = "2500002"
private const val fuelPriceSubscriptionID = "2500003"
private const val baseURL = "https://$mdmHost/BASt-MDM-Interface/srv/container/v1.0?subscriptionID="


object MtskFetcher {
    private val sslConfig: Pair<SSLSocketFactory, X509TrustManager> = SSLConfig.getSSLSocketFactory()

    fun fetchData(): List<D2LogicalModel> {
        val callRequest = { request: Request ->
            OkHttpClient
                .Builder()
                .sslSocketFactory(sslConfig.first, sslConfig.second)
                .build()
                .newCall(request)
                .execute().use { response -> responseToXmlModel(response) }
        }

        return callRequest(buildRequest(patrolStationSubscriptionID)) +
                callRequest(buildRequest(fuelPriceSubscriptionID))
    }

    private fun buildRequest(subscription: String): Request = Request.Builder().url("$baseURL$subscription").build()

    private fun responseToXmlModel(response: Response): List<D2LogicalModel> {
        return XMLParser().parse(parseResponseBodyToPayload(response.body!!.string()).getOrNull()!!)
    }

    private fun parseResponseBodyToPayload(body: String): Result<List<String>> =
        XmlMapper().registerKotlinModule().runCatching {
            readValue<Container>(body).body.binarys.map { binary ->
                GZIPInputStream(
                    Base64.decodeBase64(binary.text).inputStream()
                ).bufferedReader(StandardCharsets.UTF_8).use { it.readText() }
            }
        }

}
