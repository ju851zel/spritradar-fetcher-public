package de.formigas.spritradar.fetcher

import de.formigas.spritradar.fetcher.Commons.fromResources
import java.security.KeyStore
import java.security.SecureRandom
import javax.net.ssl.*


class SSLConfig {

    companion object {
        fun getSSLSocketFactory(): Pair<SSLSocketFactory, X509TrustManager> {
            val keyManagers = KeyManagerFactory.getInstance("SunX509", "SunJSSE").apply {
                val keyStore: KeyStore = generateKeyStore(
                    storeType = keyStoreType,
                    fileName = keyStorePath,
                    password = keyStorePassword
                )
                init(keyStore, keyStorePassword.toCharArray())
            }.keyManagers

            val trustManagers = TrustManagerFactory.getInstance("PKIX").apply {
                val trustStore: KeyStore = generateKeyStore(
                    storeType = trustStoreType,
                    fileName = trustStorePath,
                    password = trustStorePassword
                )
                init(trustStore)
            }.trustManagers

            return Pair(SSLContext.getInstance("TLS").apply {
                init(keyManagers, trustManagers, SecureRandom())
            }.socketFactory, trustManagers.first() as X509TrustManager)
        }


        private fun generateKeyStore(storeType: String, fileName: String, password: String): KeyStore =
            KeyStore.getInstance(storeType).apply {
                fromResources(fileName).use { file -> this.load(file, password.toCharArray()) }
//                object {}.javaClass.classLoader.getResourceAsStream(fileName)?
            }
    }
}


