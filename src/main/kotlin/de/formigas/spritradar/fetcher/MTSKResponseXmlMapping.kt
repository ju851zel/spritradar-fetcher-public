package de.formigas.spritradar.fetcher

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText
import java.util.*

/// These classes are used to parse the payload from the MTSK Platform into XML
/// The real content lays in the 'body' of the Container element

@JacksonXmlRootElement(localName = "identifier")
class Identifier {
    @field:JacksonXmlProperty(localName = "publicationId")
    var publicationId = 0
}

@JacksonXmlRootElement(localName = "timestamp")
class Timestamp {
    @field:JacksonXmlProperty(localName = "Created")
    var created: Date? = null

    @field:JacksonXmlProperty(localName = "Id")
    var id: String? = null

    @field:JacksonXmlProperty(localName = "Text")
    var text: Date? = null
}

@JacksonXmlRootElement(localName = "header")
class Header {
    @field:JacksonXmlProperty(localName = "Identifier")
    var identifier: Identifier? = null

    @field:JacksonXmlProperty(localName = "Timestamp")
    var timestamp: Timestamp? = null
}

@JacksonXmlRootElement(localName = "binary")
class Binary {
    @field:JacksonXmlProperty(localName = "type")
    var type: String? = null

    @field:JacksonXmlProperty(localName = "id")
    var id: String? = null

    @field:JacksonXmlText
    var text: String? = null
}

@JacksonXmlRootElement(localName = "body")
data class Body(
    @JacksonXmlProperty(localName = "binary")
    @JacksonXmlElementWrapper(useWrapping = false)
    val binarys: List<Binary>
)

@JacksonXmlRootElement(localName = "container")
data class Container(
    @field:JacksonXmlProperty(localName = "header")
    val header: Header,
    @field:JacksonXmlProperty(localName = "body")
    val body: Body
)

