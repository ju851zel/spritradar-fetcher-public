package de.formigas.spritradar.fetcher

import java.io.ByteArrayInputStream
import java.util.logging.Logger

const val CLOUD_BUCKET_ENV_VAR = "CLOUD_BUCKET"
const val CLOUD_CREDENTIALS_ENV_VAR = "CLOUD_CREDENTIALS"

// Password der P12-Datei 'mts-test.formigas.de.p12', Aktuell wird nur die Test-P12-Datei verwendet
// Art der Keystore-Datei
const val keyStoreType = "PKCS12"
const val keyStorePassword = "iF5nVXFzMC"
const val keyStorePath = "mts-test.formigas.de.p12"


// Password der Truststore-Datei die die Zertifikate der MDM-Plattform enthält
// Art der Truststore-Datei
const val trustStoreType = "JKS"
const val trustStorePath = "jssecacerts"
const val trustStorePassword = "changeit"


object Commons {
    private val logger: Logger = Logger.getLogger("Commons")

    fun ensureInitialised() {
        ensureCloudBucketEnvVarIsSet()
        ensureTrustStoreIsAvailable()
        ensureKeyStoreIsAvailable()
        ensureCloudCredentialsEnvVarIsSet()
    }

    private fun ensureTrustStoreIsAvailable() {
        fromResources(trustStorePath)
        logger.info("Using the truststore file=$trustStorePath")
    }

    private fun ensureKeyStoreIsAvailable() {
        fromResources(keyStorePath)
        logger.info("Using the keystore file=$keyStorePath")
    }

    private fun ensureCloudBucketEnvVarIsSet() {
        val bucket =
            System.getenv(CLOUD_BUCKET_ENV_VAR) ?: throw Exception("No cloud bucket env var specified for CLOUD_BUCKET")
        logger.info("Using the bucket=$bucket to write prices and stations.")
    }

    private fun ensureCloudCredentialsEnvVarIsSet() {
        System.getenv(CLOUD_CREDENTIALS_ENV_VAR)
            ?: throw Exception("No credentials env var specified for CLOUD_CREDENTIALS")
        logger.info("Using credentials to write prices and stations.")
    }

    fun fromResources(relativePath: String): ByteArrayInputStream {
        try {
            return object {}.javaClass.classLoader.getResourceAsStream(relativePath).readAllBytes().inputStream()
        } catch (e: Exception) {
            throw Exception("Could not find file or read bytes of file=$relativePath")
        }
    }
}