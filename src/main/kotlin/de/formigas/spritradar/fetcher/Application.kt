package de.formigas.spritradar.fetcher

import com.google.cloud.functions.BackgroundFunction
import com.google.cloud.functions.Context
import de.formigas.spritradar.generated.mtskinformation.PetrolStationPublication
import org.apache.commons.codec.binary.Base64
import org.joda.time.DateTime
import java.util.logging.Logger

private val logger: Logger = Logger.getLogger("Application")

class Application : BackgroundFunction<Application.PubSubMessage> {

    override fun accept(message: PubSubMessage?, context: Context?) {
        if (message?.data == null) {
            logger.warning("Message was not handled, because it was empty: $message")
            return
        }
        when (Base64.decodeBase64(message.data).toString(Charsets.UTF_8)) {
            "prices" -> fetchPricesAndWriteToObjectSore()
            "stations" -> fetchStationsAndWriteToDatabase()
            else -> logger.warning("Message was not handled, because it was not 'prices' or 'stations': $message")
        }
    }

    class PubSubMessage {
        var data: String? = null
        var attributes: Map<String, String>? = null
        var messageId: String? = null
        var publishTime: String? = null
        override fun toString(): String {
            return "PubSubMessage(data=$data, attributes=$attributes, messageId=$messageId, publishTime=$publishTime)"
        }
    }
}

// Use for local run
fun main() {
    Commons.ensureInitialised()
    fetchStationsAndWriteToDatabase()
}

fun fetchPricesAndWriteToObjectSore() {
    logger.info("Fetching prices.")
    val parsedModel = MtskFetcher.fetchData()
    val prices = parsedModel
        .asSequence()
        .filter { it.payloadPublication is de.formigas.spritradar.generated.mtskinformation.FuelPricePublication }
        .map { (it.payloadPublication as de.formigas.spritradar.generated.mtskinformation.FuelPricePublication).petrolStationInformation!! }
        .flatten()
        .partition { it.petrolStationReference?.id != null }
        .also { logger.info("There are ${it.second.count()} prices without associated petrol station ID") }
        .first
        .map {
            PetrolPrice(
                associatedStationId = it.petrolStationReference!!.id!!,
                superE5DeciCents = it.fuelPriceE5?.price?.multiply(1000.toBigDecimal())?.intValueExact(),
                superE10DeciCents = it.fuelPriceE10?.price?.multiply(1000.toBigDecimal())?.intValueExact(),
                dieselDeciCents = it.fuelPriceDiesel?.price?.multiply(1000.toBigDecimal())?.intValueExact(),
                timestampIsoString = DateTime(it.fuelPriceE5?.dateOfPrice?.toGregorianCalendar()?.time).toString(),
            )
        }

    FirebaseStore.savePrices(prices)
}

fun fetchStationsAndWriteToDatabase() {
    logger.info("Fetching stations.")
    val parsedModel = MtskFetcher.fetchData()
    val stations = parsedModel
        .asSequence()
        .filter { it.payloadPublication is PetrolStationPublication }
        .map { (it.payloadPublication as PetrolStationPublication).petrolStation ?: listOf() }
        .flatten()
        .partition { it.id != null }
        .also { logger.info("There are ${it.second.count()} stations without ID") }
        .first
        .map {
            PetrolStationModel(
                id = it.id ?: "",
                name = it.petrolStationName ?: "",
                brand = it.petrolStationBrand ?: "",
                street = it.petrolStationStreet ?: "",
                houseNr = it.petrolStationHouseNumber ?: "",
                zip = it.petrolStationPostcode ?: "",
                city = it.petrolStationPlace ?: "",
                position = it.petrolStationLocation?.let { point -> LatLng(point.latitude, point.longitude) } ?: LatLng(
                    -1f,
                    -1f
                ),
                openingHours = mapOf(),
                currentPrice = null
            )
        }
        .filter { it.id.isNotBlank() && it.zip.isNotBlank() && it.position.let { pos -> pos.lat > 0 && pos.lon > 0 } }
    FirebaseStore.saveStations(stations)
}

//fun determineOpeningHours(openingHours: List<List<Period>>) {
// TODO: parse opening hours and save them with the station
// val openingHours = parsedModel
//     .asSequence()
//     .filter { it.payloadPublication is PetrolStationPublication }
//     .map { (it.payloadPublication as PetrolStationPublication).petrolStation ?: listOf() }
//     .flatten().map { it.openingTimes }
//     .toList()
//     Files.readString(
//         Path("/Users/julian/HTWGProjects/spritradar-data-fetcher/src/main/resources/example.xml"),
//     )
// determineOpeningHours(openingHours.filterNotNull().map { it })

// for the model create the view like so for each station
// examples:
// Mo-Mi 06:00 - 18:00, Do 07:00 - 20:00, Fr-So: 08:00 - 23:00,
// Mo-So 00:00 - 00:00,
// Mo-Fr 00:00 - 20:00
//    openingHours.forEach { days ->
//        if (days.count() == 7) {
//            val days = days.map {
//                val day = ((it as PeriodV1x1x0).recurringDayWeekMonthPeriod as DayWeekMonth).applicableDay
//                val start = (it.recurringTimePeriodOfDay?.get(0) as TimePeriodByHour).startTimeOfPeriod
//                val end = (it.recurringTimePeriodOfDay?.get(0) as TimePeriodByHour).endTimeOfPeriod
//                Triple(day, start, end)
//            }.zipWithNext()
//            println(days)
//        }
//    }
//}
