package de.formigas.spritradar.fetcher

import com.google.auth.oauth2.ServiceAccountCredentials
import com.google.cloud.firestore.Firestore
import com.google.cloud.firestore.FirestoreOptions
import com.google.cloud.storage.BlobId
import com.google.cloud.storage.BlobInfo
import com.google.cloud.storage.Storage
import com.google.cloud.storage.StorageOptions
import com.google.gson.Gson
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter


private const val STORAGE_FILE_TYPE = "text/json"

object FirebaseStore {
    private val firestore: Firestore
    private val storage: Storage
    private val CLOUD_BUCKET: String = System.getenv(CLOUD_BUCKET_ENV_VAR)
        ?: throw Exception("no cloud bucket env var specified for CLOUD_BUCKET")
    private val CREDENTIALS: String = System.getenv(CLOUD_CREDENTIALS_ENV_VAR)
        ?: throw Exception("no cloud bucket env var specified for CLOUD_CREDENTIALS")

    init {
        ServiceAccountCredentials.fromStream(CREDENTIALS.byteInputStream(Charsets.UTF_8)).also { credentials ->
            firestore = FirestoreOptions.newBuilder().setCredentials(credentials).build().service
            storage = StorageOptions.newBuilder().setCredentials(credentials).build().service
        }
    }

    fun saveStations(data: List<PetrolStationModel>) {
        data.map { element -> element.copy(currentPrice = null) }
            .chunked(500).forEach { list ->
                val result = list.fold(firestore.batch()) { batch, it ->
                    batch.set(firestore.collection("stations").document(it.id), it)
                }.commit()
                print(result.get())
            }

    }

    fun savePrices(data: List<PetrolPrice>) {
        BlobInfo.newBuilder(BlobId.of(CLOUD_BUCKET, getIsoDateTimeNowMinutePrecision()))
            .setContentType(STORAGE_FILE_TYPE)
            .build().also { blobInfo ->
                storage.create(blobInfo, Gson().toJson(data).toByteArray(Charsets.UTF_8))
            }
    }

    private fun getIsoDateTimeNowMinutePrecision(): String =
        ZonedDateTime.now(ZoneOffset.UTC).withSecond(0).withNano(0).format(DateTimeFormatter.ISO_INSTANT)
}

